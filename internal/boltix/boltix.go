package boltix

import (
	"context"
	"encoding/binary"
	"errors"
	"os"
	"path/filepath"
	"strconv"

	"go.etcd.io/bbolt"
)

const maxSize = 65536

type Indexer struct {
	objPath string
	db      *bbolt.DB
	tx      *bbolt.Tx
	bucket  *bbolt.Bucket
	dbPath  string
	tmpDir  string
}

func New(objPath string, dbPath string) (*Indexer, error) {
	// TODO - embed version?

	// build into a temp-file
	tmpDir, err := os.MkdirTemp("", "db")
	if err != nil {
		return nil, err
	}
	tmpFileName := filepath.Join(tmpDir, "db")

	db, err := bbolt.Open(tmpFileName, 0600, nil)
	if err != nil {
		return nil, err
	}

	db.NoSync = true
	db.NoFreelistSync = true
	db.NoGrowSync = true

	return &Indexer{
		objPath: objPath,
		db:      db,
		tmpDir:  tmpDir,
		dbPath:  dbPath,
	}, nil
}

func (i *Indexer) Index(ctx context.Context, offset int64, ln int, typ string, val string) error {
	if i.bucket == nil {
		tx, err := i.db.Begin(true)
		if err != nil {
			return err
		}
		bucket, err := tx.CreateBucketIfNotExists([]byte("vals"))
		if err != nil {
			tx.Rollback()
		}

		i.tx = tx
		i.bucket = bucket
	}

	b := i.bucket
	nextId, err := b.NextSequence()
	if err != nil {
		return err
	}

	k := make([]byte, 0, len(typ)+len(val)+6)
	k = append(k, typ...)
	k = append(k, ':')
	k = append(k, val...)
	k = append(k, ':')
	k = strconv.AppendInt(k, int64(nextId), 10)

	// TODO - something better?
	v := make([]byte, 0, len(i.objPath)+6)
	v = binary.AppendUvarint(v, uint64(len(i.objPath)))
	v = append(v, i.objPath...)
	v = binary.AppendUvarint(v, uint64(offset))
	v = binary.AppendUvarint(v, uint64(ln))

	return b.Put(k, v)
}

func (i *Indexer) Flush(ctx context.Context) error {
	if i.tx == nil {
		return nil
	}
	err := i.tx.Commit()
	if err != nil {
		return err
	}

	i.tx = nil
	i.bucket = nil

	return nil
}

func (i *Indexer) Close(ctx context.Context) error {
	defer func() {
		_ = os.RemoveAll(i.tmpDir)
	}()

	err := i.Flush(ctx)
	if err != nil {
		return err
	}

	// open real file
	_ = os.Remove(i.dbPath)
	dstDb, err := bbolt.Open(i.dbPath, 0600, &bbolt.Options{
		NoSync: true,
	})
	if err != nil {
		return err
	}
	err = bbolt.Compact(dstDb, i.db, maxSize)
	if err != nil {
		return nil
	}
	err1 := dstDb.Close()
	err2 := i.db.Close()
	err3 := os.RemoveAll(i.tmpDir)

	return errors.Join(err1, err2, err3)
}
