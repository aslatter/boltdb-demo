package sqlix

import (
	"context"
	"database/sql"
	"fmt"

	_ "modernc.org/sqlite"
)

type Indexer struct {
	db   *sql.DB
	tx   *sql.Tx
	path string
}

func New(objPath string, dbPath string) (*Indexer, error) {
	db, err := createDb(dbPath)
	if err != nil {
		return nil, err
	}
	return &Indexer{
		db:   db,
		path: objPath,
	}, nil
}

func (i *Indexer) Index(ctx context.Context, offset int64, len int, typ string, val string) error {
	if i.tx == nil {
		tx, err := i.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		i.tx = tx
	}

	_, err := i.tx.Exec(
		`INSERT INTO vals (keytype, keyval, offset, len) VALUES (?, ?, ?, ?)`,
		typ, val, offset, len,
	)
	return err
}

func (i *Indexer) Flush(ctx context.Context) error {
	if i.tx == nil {
		return nil
	}
	err := i.tx.Commit()
	if err != nil {
		return err
	}
	i.tx = nil

	return nil
}

func createDb(path string) (db *sql.DB, err error) {
	defer func() {
		if db == nil || err == nil {
			return
		}
		_ = db.Close()
	}()

	// TODO - consider removing the file at 'path'

	db, err = sql.Open("sqlite", path)
	if err != nil {
		err = fmt.Errorf("opening database: %s", err)
		return
	}

	_, err = db.Exec(`DROP TABLE IF EXISTS vals`)
	if err != nil {
		// TODO
		return
	}

	// including the path in the table increases the size by >20%,
	// but after compression the increase is not significant.
	//
	// no pkey, but we can gaurantee uniqueness by construction.
	_, err = db.Exec(`
		CREATE TABLE vals (
			keytype TEXT not null,
			keyval TEXT not null,
			offset INTEGER not null,
			len INTEGER not null
		)
	`)
	if err != nil {
		return
	}
	_, err = db.Exec(`
		CREATE index vals_index ON vals (keytype, keyval)
	`)
	return
}
