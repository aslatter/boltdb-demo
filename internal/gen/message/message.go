package message

import (
	"context"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"io"
	"math/rand"
)

type Indexer interface {
	Index(ctx context.Context, offset int64, len int, typ string, val string) error
	Flush(ctx context.Context) error
}

// Generate builds dummy JSON documents, newline delimitted, to stdout.
// Each document has:
//   - A unique id (16-bytes, base64)
//   - "subj1": 20k cardinality
//   - "subj2": 200 cardinality
//   - 20 additional properties whose keys have a cardinality of 200,
//     and whose values have a cardinality of 2k.
//
// Each JSON document (plus newline) corresponds to a single call to w.Write.
func Generate(ctx context.Context, count int, w io.Writer, ix Indexer) error {
	var offset int64
	var flushCounter int

	for i := 0; i < count; i++ {
		if ctx.Err() != nil {
			return ctx.Err()
		}
		arr, ixs := genOne(20)
		l, err := w.Write(arr)
		if err != nil {
			return err
		}

		for _, i := range ixs {
			err = ix.Index(ctx, offset, l, i.k, i.v)
			if err != nil {
				return err
			}
			flushCounter++
			if flushCounter >= 100 {
				flushCounter = 0
				err = ix.Flush(ctx)
				if err != nil {
					return err
				}
			}
		}
		offset += int64(l)
	}
	err := ix.Flush(ctx)
	if err != nil {
		return err
	}

	if closer, ok := ix.(interface{ Close(context.Context) error }); ok {
		err = closer.Close(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

type ixval struct {
	id, k, v string
}

func genOne(propCount int) (_ []byte, ix []ixval) {
	idBytes := make([]byte, 16)
	_, _ = rand.Read(idBytes)
	idStr := base64.RawURLEncoding.EncodeToString(idBytes)

	subj1 := base64.RawURLEncoding.EncodeToString(binary.AppendVarint(nil, int64(rand.Int31n(20_000))))
	subj2 := base64.RawURLEncoding.EncodeToString(binary.AppendVarint(nil, int64(rand.Int31n(200))))

	vals := make(map[string]string, propCount+2)
	vals["id"] = idStr
	vals["subj1"] = subj1
	vals["subj2"] = subj2

	ix = append(ix, ixval{id: idStr, k: "subj1", v: subj1})
	ix = append(ix, ixval{id: idStr, k: "subj2", v: subj2})

	for i := 0; i < propCount; i++ {
		key := base64.RawURLEncoding.EncodeToString(binary.AppendVarint(nil, int64(rand.Int31n(200))))
		val := base64.RawURLEncoding.EncodeToString(binary.AppendVarint(nil, int64(rand.Int31n(2_000))))
		vals[key] = val
	}
	bytes, _ := json.Marshal(vals)
	bytes = append(bytes, '\n')

	return bytes, ix
}
