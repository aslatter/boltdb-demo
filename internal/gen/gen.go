package gen

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"math/rand"
)

func RandomKey(b []byte, distincKeys int32) []byte {
	out := bytes.NewBuffer(b)
	out.Reset()

	s := make([]byte, 0, 8)

	// fixed prefix
	out.WriteString("aaa")

	// small number of possible subjects
	id := rand.Int31n(distincKeys)
	enc := base64.NewEncoder(base64.RawStdEncoding, out)
	enc.Write(binary.LittleEndian.AppendUint32(s, uint32(id)))

	// random suffix
	s = s[:8]
	rand.Read(s)
	enc.Write(s)

	enc.Close()

	return out.Bytes()
}

func RandomValue(b []byte) {
	rand.Read(b)
}
