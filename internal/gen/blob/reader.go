package blob

import (
	"context"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go/service/s3"
)

type SeekableReader interface {
	io.ReadSeeker
	io.ReaderAt
}

type LengthReader interface {
	io.ReaderAt
	ContentLength() (int64, error)
}

type seekableReader struct {
	r   LengthReader
	pos int64
}

// Read implements SeekableReader.
func (r *seekableReader) Read(p []byte) (int, error) {
	n, err := r.ReadAt(p, r.pos)
	r.pos += int64(n)
	return n, err
}

// ReadAt implements SeekableReader.
func (r *seekableReader) ReadAt(p []byte, off int64) (n int, err error) {
	return r.r.ReadAt(p, off)
}

// Seek implements SeekableReader.
func (r *seekableReader) Seek(offset int64, whence int) (int64, error) {
	// handle cases where we don't need to fetch the length
	if offset == 0 && whence == io.SeekStart {
		r.pos = 0
		return 0, nil
	}
	if offset <= 0 && whence == io.SeekCurrent {
		if r.pos+offset < 0 {
			return 0, fmt.Errorf("cannot seek to before start of file")
		}
		return r.pos, nil
	}

	// handle cases where we need to know the length
	fileLen, err := r.r.ContentLength()
	if err != nil {
		return 0, err
	}

	var newPos int64

	switch whence {
	case io.SeekStart:
		newPos = int64(whence)
	case io.SeekCurrent:
		newPos = r.pos + int64(whence)
	case io.SeekEnd:
		newPos = fileLen + int64(whence)
	}

	if newPos < 0 {
		return 0, fmt.Errorf("cannot seek to before start of file")
	}
	if newPos > fileLen {
		newPos = fileLen
	}

	r.pos = newPos
	return r.pos, nil
}

// NewPositionedReader converts a 'ReaderAt' into a 'Reader'.
func NewPositionedReader(r LengthReader) SeekableReader {
	return &seekableReader{r: r}
}

// S3Reader implements a reader for an S3 object in the simplest
// possible way. No object-details are cached in between calls.
type S3Reader struct {
	ctx    context.Context
	client *s3.S3
	bucket string
	key    string
	// TODO - per operation timeout?
}

func NewS3Reader(ctx context.Context, client *s3.S3, bucket string, key string) *S3Reader {
	return &S3Reader{
		ctx:    ctx,
		client: client,
		bucket: bucket,
		key:    key,
	}
}

// ReadAt implements io.ReaderAt.
func (r *S3Reader) ReadAt(p []byte, off int64) (n int, err error) {

	rng := fmt.Sprintf("bytes=%d-%d", off, off+int64(len(p)))

	resp, err := r.client.GetObjectWithContext(r.ctx, &s3.GetObjectInput{
		Bucket: &r.bucket,
		Key:    &r.key,
		Range:  &rng,
	})
	if err != nil {
		return 0, err
	}

	defer resp.Body.Close()

	// the body returned by range-requests is magic and returns EOF
	// exactly when we want it to, somehow. i.e. if the range returned
	// was not inclusive of the end of the file, we do not return EOF,
	// but if it was we do. I have not dug in to why.
	return resp.Body.Read(p)
}

func (r *S3Reader) ContentLength() (int64, error) {
	// handle cases where we need to know the length
	resp, err := r.client.HeadObjectWithContext(r.ctx, &s3.HeadObjectInput{
		Bucket: &r.bucket,
		Key:    &r.key,
	})
	if err != nil {
		return 0, err
	}
	if resp.ContentLength == nil {
		return 0, fmt.Errorf("head object response did not include a content-length")
	}
	return *resp.ContentLength, nil
}

var _ io.ReaderAt = (*S3Reader)(nil)
