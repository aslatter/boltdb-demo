package blob

import (
	"bytes"
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go/service/s3"
)

// S3Writer is a thin wrapper around S3 multi-part uploads. Each call
// to Write corresponds to an upload of a part. The upload is finalized
// when Close is called.
type S3Writer struct {
	ctx         context.Context
	client      *s3.S3
	bucket      string
	key         string
	multiPartId string
	n           uint32
	partList    []string
}

func NewS3Writer(ctx context.Context, client *s3.S3, bucket string, key string) *S3Writer {
	return &S3Writer{
		ctx:    ctx,
		client: client,
		bucket: bucket,
		key:    key,
		// TODO - operation timeout?
	}
}

func (w *S3Writer) Write(p []byte) (int, error) {
	err := w.write(p)
	if err != nil {
		return 0, err
	}
	return len(p), nil
}

func (w *S3Writer) write(p []byte) error {
	if len(p) == 0 {
		return nil
	}

	if w.multiPartId == "" {
		r, err := w.client.CreateMultipartUploadWithContext(w.ctx, &s3.CreateMultipartUploadInput{
			Bucket: &w.bucket,
			Key:    &w.key,
		})
		if err != nil {
			return err
		}
		if r.UploadId == nil {
			return fmt.Errorf("create-multipart response did not include an upload id")
		}
		w.multiPartId = *r.UploadId
	}

	w.n++
	var partNumber int64 = int64(w.n)

	partResponse, err := w.client.UploadPartWithContext(w.ctx, &s3.UploadPartInput{
		Body:       bytes.NewReader(p),
		Bucket:     &w.bucket,
		Key:        &w.key,
		PartNumber: &partNumber,
		UploadId:   &w.multiPartId,
	})
	if err != nil {
		return err
	}

	if partResponse.ETag == nil || *partResponse.ETag == "" {
		return fmt.Errorf("expected upload-part response to include an etag")
	}

	w.partList = append(w.partList, *partResponse.ETag)

	return nil
}

func (w *S3Writer) Close() error {

	if len(w.partList) == 0 {
		return nil
	}

	var partList []*s3.CompletedPart
	for ix, part := range w.partList {
		number := int64(ix) + 1
		etag := part
		partList = append(partList, &s3.CompletedPart{
			ETag:       &etag,
			PartNumber: &number,
		})
	}

	// allow double-close
	w.partList = nil

	_, err := w.client.CompleteMultipartUploadWithContext(w.ctx, &s3.CompleteMultipartUploadInput{
		Bucket: &w.bucket,
		Key:    &w.key,
		MultipartUpload: &s3.CompletedMultipartUpload{
			Parts: partList,
		},
		UploadId: &w.multiPartId,
	})
	if err != nil {
		return err
	}

	return nil
}
