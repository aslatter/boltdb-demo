# boltdb demo

This repo includes two small executables which experiment
with creating some sort of key-value index-structure with
both sqlite and boltdb.

The two commands are:

* `go run ./cmd/genbolt`
* `go run ./cmd/gensql`

They produced a key-value database with keys of the form:

```
<fixed prefix> <subject id> <event id>
```

The payload in the database is 60 random bytes.

The 'subject id' is 32 bytes, base64 encoded.

The 'event id' is 64 bytes, base64 encoded.

By default we will creat 1,000,000 events, with
70,000 distinct subjects.

The resulting database will be *zstd* compressed
before being written to disk.

Example:

```
go run ./cmd/genboltdb -db=test.db.zstd
```

or:

```
go run ./cmd/gensql -db=test.db.zstd
```

You can uncompress a db file with the *unzstd* utility:

```
unzstd test.db.zstd
```
