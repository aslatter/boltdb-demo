package main

import (
	"bytes"
	"fmt"

	"github.com/spf13/cobra"
	bolt "go.etcd.io/bbolt"
)

func buildListKeysCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use: "list-keys",
		RunE: func(cmd *cobra.Command, args []string) error {
			dbPath := dbKey()

			db, err := bolt.Open(dbPath, 0600, nil)
			if err != nil {
				return err
			}
			defer db.Close()

			tx, err := db.Begin(false)
			if err != nil {
				return err
			}
			defer tx.Rollback()

			b := tx.Bucket([]byte("vals"))
			if b == nil {
				return fmt.Errorf("could not find bucket 'vals'")
			}

			c := b.Cursor()
			k := nextKey(c, nil)
			for k != nil {
				fmt.Printf("%s\n", k)
				k = nextKey(c, k)
			}

			return nil
		},
	}
	return cmd
}

func extractIndexKey(dbKey []byte) []byte {
	pieces := bytes.SplitN(dbKey, []byte(":"), 3)
	b := append([]byte(nil), pieces[0]...)
	b = append(b, ':')
	b = append(b, pieces[1]...)
	return b
}

func nextKey(c *bolt.Cursor, lastKey []byte) []byte {
	var n []byte

	if lastKey == nil {
		n, _ = c.First()
	} else {
		afterCurrent := append(lastKey, ':'+1)
		n, _ = c.Seek(afterCurrent)
	}
	if n == nil {
		return nil
	}
	return extractIndexKey(n)
}
