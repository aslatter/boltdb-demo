package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/spf13/cobra"
	"golang.org/x/sys/unix"
)

func main() {
	ctx, close := signal.NotifyContext(context.Background(), os.Interrupt, unix.SIGTERM)
	defer close()

	cmd := buildRootCommand()

	err := cmd.ExecuteContext(ctx)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error: ", err)
		os.Exit(1)
	}
}

var sess *session.Session
var bucket string
var key string

func buildRootCommand() *cobra.Command {
	cmd := &cobra.Command{
		SilenceErrors: true,
		SilenceUsage:  true,
	}

	var region string
	var awsTrace bool

	f := cmd.PersistentFlags()
	f.StringVar(&region, "region", "", "AWS region")
	f.StringVar(&bucket, "bucket", "", "S3 bucket")
	f.StringVar(&key, "key", "", "S3 object key")
	f.BoolVar(&awsTrace, "aws-trace", false, "trace AWS calls")

	cmd.MarkPersistentFlagRequired("region")
	cmd.MarkPersistentFlagRequired("bucket")
	cmd.MarkPersistentFlagRequired("key")

	cmd.PersistentPreRunE = func(cmd *cobra.Command, _ []string) error {
		var err error
		sess, err = session.NewSession(&aws.Config{Region: &region})
		if err != nil {
			return err
		}

		if awsTrace {
			sess.Handlers.Complete.PushBack(func(r *request.Request) {
				log.Info("aws-call",
					"op", r.Operation.Name,
					"status", r.HTTPResponse.StatusCode,
					"reqLen", r.HTTPRequest.ContentLength,
					"respLen", r.HTTPResponse.ContentLength,
				)
			})
		}

		return nil
	}

	cmd.AddCommand(
		buildS3TestCommand(),
		buildBasicGenCommand(),
		buildListKeysCommand(),
	)

	return cmd
}
