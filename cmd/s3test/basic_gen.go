package main

import (
	"bufio"
	"context"
	"errors"
	"io"
	"os"
	"strings"

	"bboltdemo/internal/boltix"
	"bboltdemo/internal/gen/blob"
	"bboltdemo/internal/gen/message"

	seekable "github.com/SaveTheRbtz/zstd-seekable-format-go"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/klauspost/compress/zstd"
	"github.com/spf13/cobra"
)

func buildBasicGenCommand() *cobra.Command {
	opts := basicGenOpts{
		count: 1_000_000,
	}
	cmd := &cobra.Command{
		Use:   "basic-gen",
		Short: "generate bulk test data",
		RunE: func(cmd *cobra.Command, _ []string) error {
			return basicGen(cmd.Context(), &opts)
		},
	}

	cmd.Flags().BoolVar(&opts.local, "local", opts.local, "generate to stdout not s3")
	cmd.Flags().IntVar(&opts.count, "count", opts.count, "number of events to generate")
	cmd.Flags().BoolVar(&opts.skipCompress, "skip-compress", opts.skipCompress, "do not zstd compressing result")

	return cmd
}

type basicGenOpts struct {
	local        bool
	skipCompress bool
	count        int
}

func basicGen(ctx context.Context, opts *basicGenOpts) (err error) {
	var errList []error
	defer func() {
		if err != nil {
			errList = append(errList, err)
		}
		err = errors.Join(errList...)
	}()

	var w io.Writer
	if !opts.local {
		svc := s3.New(sess)

		// make a new blob
		sw := blob.NewS3Writer(ctx, svc, bucket, key)
		defer func() {
			errList = append(errList, sw.Close())
		}()

		// Each call to sw.Write corresponds to an S3 API call.
		// To keep this under control we introduce a layer of buffering.
		bw := bufio.NewWriterSize(sw, 20*1024*1024)
		defer func() {
			errList = append(errList, bw.Flush())
		}()

		w = bw
	} else {
		f, err := os.OpenFile(key, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
		if err != nil {
			return err
		}
		defer func() {
			errList = append(errList, f.Close())
		}()
		w = f
	}

	if !opts.skipCompress {
		zenc, err := zstd.NewWriter(nil, zstd.WithEncoderLevel(zstd.SpeedBestCompression))
		if err != nil {
			return err
		}
		zw, err := seekable.NewWriter(w, zenc)
		if err != nil {
			return err
		}
		defer func() {
			errList = append(errList, zw.Close())
		}()
		w = zw

		// each call to zw.Write creates a "frame" within the seekable
		// data-structure. Under the covers the seekable-writer is building
		// a b-tree map of frame-locations within the compressed stream. This
		// means we want to limit/control the size of these frames. We do this
		// with another buffered-writer, to limit the number of calls to 'Write'
		// in the seekable-stream-writer.
		bw := bufio.NewWriterSize(w, 512*1024)
		defer func() {
			errList = append(errList, bw.Flush())
		}()
		w = bw
	}

	// build index
	indexer, err := boltix.New(key, dbKey())
	if err != nil {
		return err
	}
	defer func() {
		errList = append(errList, indexer.Flush(ctx))
	}()

	return message.Generate(ctx, opts.count, w, indexer)
}

func dbKey() string {
	origPath := key
	origPath = strings.TrimSuffix(origPath, ".zst")
	return origPath + ".index.db"
}
