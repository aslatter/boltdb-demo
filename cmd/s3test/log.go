package main

import (
	"os"
	"time"

	"log/slog"
)

var log *slog.Logger

func init() {
	log = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			// custom time format
			if len(groups) == 0 && a.Key == "time" && a.Value.Kind() == slog.KindTime {
				return slog.String("time", a.Value.Time().Format(time.TimeOnly))
			}
			return a
		},
	}))
}
