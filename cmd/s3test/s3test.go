package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"

	"bboltdemo/internal/gen/blob"

	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/klauspost/compress/zstd"
	"github.com/spf13/cobra"
)

func buildS3TestCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "s3test",
		Short: "demo of s3",
		Long:  "Upload and download compressed demo-data",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			return s3Test(cmd.Context())
		},
	}
}

func s3Test(ctx context.Context) error {

	svc := s3.New(sess)

	// make a new blob
	sw := blob.NewS3Writer(ctx, svc, bucket, key)
	bw := bufio.NewWriterSize(sw, 20*1024*1024)
	w, err := zstd.NewWriter(bw)
	if err != nil {
		return err
	}

	fmt.Fprintln(w, "hello, world!")
	fmt.Fprintln(w, "The quick brown fox jumps over the lazy dog")
	fmt.Fprintln(w, "Okay?")

	// fz, err := os.Open("test.zeros")
	// if err != nil {
	// 	return err
	// }
	// _, err = io.Copy(w, fz)
	// if err != nil {
	// 	return err
	// }

	err = w.Close()
	if err != nil {
		return err
	}
	err = bw.Flush()
	if err != nil {
		return err
	}
	err = sw.Close()
	if err != nil {
		return err
	}

	// check the blob details
	sr := blob.NewS3Reader(ctx, svc, bucket, key)
	r := blob.NewPositionedReader(sr)
	len, err := r.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}
	_, err = r.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	fmt.Println("length of blob: ", len)

	b := make([]byte, 0)
	n, err := r.ReadAt(b, 200)

	fmt.Println("read count: ", n)
	fmt.Println("read error: ", err)
	fmt.Printf("error type: %T\n", err)

	zr, err := zstd.NewReader(bufio.NewReaderSize(r, 20*1024))
	if err != nil {
		return err
	}

	_, err = io.Copy(os.Stdout, io.LimitReader(zr, 1024))
	if err != nil {
		return err
	}

	return nil
}
