package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"database/sql"

	_ "modernc.org/sqlite"

	"bboltdemo/internal/gen"

	"github.com/klauspost/compress/zstd"
)

func main() {
	err := mainErr()
	if err != nil {
		fmt.Fprintln(os.Stderr, "error: ", err)
		os.Exit(1)
	}
}

var distincKeys = 70_000
var eventCount = 1_000_000

func mainErr() error {
	dbFile := flag.String("db", "", "path to database file")
	flag.IntVar(&distincKeys, "distincKeys", distincKeys, "number of subjects the events are about")
	flag.IntVar(&eventCount, "eventCount", eventCount, "number of events to generate")
	flag.Parse()

	if *dbFile == "" {
		return fmt.Errorf("flag 'db' is required")
	}

	tmpFile, err := os.CreateTemp("", "db")
	if err != nil {
		return fmt.Errorf("unable to create temp file: %s", err)
	}

	tmpName := tmpFile.Name()
	defer func() {
		tmpFile.Close()
		os.Remove(tmpName)
	}()
	tmpFile.Close()

	db, err := sql.Open("sqlite", tmpName)
	if err != nil {
		return fmt.Errorf("opening database: %s", err)
	}
	defer db.Close()

	log.Println("generating data")

	// fill database with random keys
	err = fillDb(db)
	if err != nil {
		return err
	}

	db.Close()

	log.Println("compressing data")

	destFile, err := os.OpenFile(*dbFile, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return fmt.Errorf("opening destination file: %s", err)
	}
	defer destFile.Close()

	tmp, err := os.Open(tmpName)
	if err != nil {
		return fmt.Errorf("re-opneing built database: %s", err)
	}
	defer tmp.Close()

	// compress database
	zw, err := zstd.NewWriter(destFile, zstd.WithEncoderLevel(zstd.SpeedBetterCompression))
	if err != nil {
		return fmt.Errorf("creating zstd encoder: %s", err)
	}
	defer zw.Close()

	_, err = io.Copy(zw, tmp)
	if err != nil {
		return err
	}

	log.Println("done")

	return nil
}

func fillDb(db *sql.DB) error {

	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, err = tx.Exec(`
		CREATE TABLE vals (
			key TEXT PRIMARY KEY,
			val BLOB NOT NULL
		) WITHOUT ROWID
	`)
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}

	// exmaple payload:
	//  2023/09/21/chunk1.dat:10000000
	var k []byte
	v := make([]byte, 60)

	remainingEvents := eventCount
	for remainingEvents > 0 {
		batchSize := 10_000
		if batchSize > remainingEvents {
			batchSize = remainingEvents
		}

		// do update
		tx, err = db.Begin()
		if err != nil {
			return err
		}
		defer tx.Rollback()

		for i := 0; i < batchSize; i++ {
			k = gen.RandomKey(k, int32(distincKeys))
			gen.RandomValue(v)

			_, err = tx.Exec(`INSERT INTO vals (key, val) VALUES (?, ?)`, k, v)
			if err != nil {
				return err
			}
		}
		err = tx.Commit()
		if err != nil {
			return err
		}

		remainingEvents -= batchSize
	}

	return nil
}
